var package = require('./src/res/contenu.json'),
property = package[0];

console.log(property);
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        compress : false,
        mangle : false
      },
      build: {
        src: 'src/js/**/\@*.js',
        dest: 'site/public/static/js/script.min.js'
      }
    },
    less: {
     all: {
      src: 'src/css/*.less',
      dest: 'site/public/static/css/all.css',
      options: {
        compress: true
      }
    }
  },
  jade: {
  html: {
    files: {
      'site/public/index.html': ['src/layout.jade'],
      'site/public/probleme.html': ['src/probleme.jade']
    },
    options: {
      client: false,
      data :  property
  }
  }
},
watch: {
  scripts: {
    files: ['src/**/*','!(**/**/@(.*).js)'],
    tasks: ['complet'],
    options: {
      nospawn: true
    }
  }
}

});

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Default task(s).
  grunt.registerTask('default', ['less',"jade","watch"]);
  grunt.registerTask('complet', ['uglify',"less","jade","watch"]);

};
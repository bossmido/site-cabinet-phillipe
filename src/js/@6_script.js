    jQuery =  $;
    $(document).ready(function() {
    //mis en place de deffered pour zepto
    Deferred.installInto($);
    //ajouter svg a décoration
    jQuery.browser =  {};

    $('.decoration').svg({loadURL: 'static/img/test.svg'});
    //equilibre la taille des colonnes de titres et d'infos
    var max_height = 0;
    $(".centre").each(function(){
      if ($(this).height() > max_height) { max_height = $(this).height(); }
    });  
    $(".centre").height(max_height);
    //utilisatition du plugin de fitext pour le titre et le contenu
    window.fitText(    $(".titre ")[0],5.1);

    //adaptation a l'ecran coté animation

    if($(window).width()<700){
      $('.titre').css("min-width",$(window).width()-11);
    }
    //adaptation du piti bonhomme
    if($(window).width()<1024){
      $('#pro').css("top",(-($(".conteneur_double").offset().top + $(".vrai_contenu").offset().top + $(".titre").height() )));
      $('#pro').parent().width(500+"px");
      $('#pro').parent().height(500+"px");
      $('#pro').css("min-width",$(document).width());

      $("#pro").attr("src",   $("#pro").attr("src"));

      var p  =$('#pro').parent();
      $('#pro').remove().appendTo(p);
      $(".conteneur_double").css("min-width","100%");

    }else{
     $('.decoration').css("position","absolute");
       //$('.decoration').css("top",(-($(".conteneur_double").offset().top + $(".vrai_contenu").offset().top + $(".titre").height() )));
       $('.decoration').css("left",1150+"px");
       $('.decoration').css("top",180+"px");
     }



    //slide d'arrivé du titre

    $('body').css("overflow","hidden");
    $('.titre').css("left",$(window).width()+"");
    var estarrive = false;
    var mover = $('.titre');
    current = 1,
    taille = $(window).width(),
    ticks = $(window).width();
    mover.css("left",$(window).width()+"px");

    var deferred = $.Deferred();
    var promise = deferred.promise();



    var anim = setInterval(function(){
      if (current >= ticks){
        deferred.resolve();
        estarrive  = true;
        clearInterval(anim);

      }
      else {
        mover.css('left',taille-current);
        current+=30;
      }
    },50);

    promise.done(function () {
   //fin des animations
   $(".sous_info").animate({
    opacity: 1,
    height: '+=100',
    height: 'toggle'
  }, 5000/*,'InOutBounce'*/,function() {

  });
   var totalHeight =1000;
   $(".titre").children().each(function(){
    totalHeight = totalHeight + $(this).outerHeight();
  });
   mover.slideDown(500);
   var vitesse = 5000;
   var pas = vitesse/totalHeight;
   $(".centre").animate({
    opacity: 1,
    'margin-top': '+='+pas,
       // height: 'toggle'
     }, vitesse/*,'InOutBounce'*/,function() {
      var totalHeight_page=0;
      $(".conteneur_double").children().each(function(){

        totalHeight_page = totalHeight_page + $(this).outerHeight();
      });
  //gestion d'évènement
  var ancien = $(".conteneur_double").height();
  $(".conteneur_double").height(totalHeight_page);
  $(".conteneur_double").height(totalHeight_page);
  $(".vrai_contenu").height(ancien);
  $("#overlay").height($(document).height());
  $("#lines").height($(document).height());

  $(".bloc_droit").fadeIn(5000);

  $(".ombre_sous_titre").css({"top":($(".titre").height())+"px"});
});
 });

  });

    $(document).ready(function(){
      var apparu  = false;
      var total_hauteur_choix_contact=75;
      $(".choix_contact").children().each(function(){

        total_hauteur_choix_contact = total_hauteur_choix_contact + $(this).outerHeight();
      });
      $("#contactLink").click(function(){

       if (!apparu){
        console.log("process initiated");
    // $("#contactForm").slideDown("slow");
    // $(".choix_contact").css("background-color","blue");
    $(".choix_contact").animate({top:$(".titre").height()},"slow");
     //$(".titre").animate({height:900},500);
     $(".choix_contact").animate({height:total_hauteur_choix_contact},"slow");
     apparu = true;
   }
   else{
    $(".choix_contact").stop(true,true).animate({top:(-$(".titre").height())},"slow");
    apparu= false;
  }
});
    });
    function closeForm(){
     $("#messageSent").show("slow");
     setTimeout('$("#messageSent").hide();$("#contactForm").slideUp("slow")', 2000);
   }
